// dangerous global variable (only for temporal use)
var globals = {
    newID: null,
    newDesk: null
}

/// parameters

var constants = {
    radius: { A: 10, B:5 }
}

var width  = 600, // original value: 960,
    height = 300, // original value: 500
    colors = d3.scale.category10();

var defaultNodes= [
	{id: 0, reflexive: false, type: "A", desc: "風立ちぬ", index:2},
	{id: 1, reflexive: true, type: "B", index:2},
	{id: 2, reflexive: false, type: "A", desc: "数式$\LaTeX$テスト", index:3}
    // *init problem*
    // single backslash '\' is neglected (removed) while loading.
    // double backslash '\\' causes entire (unknown) problem in saving/loading
    ] /// not json, but a *list* of json
var defaultLastNodeId= 2
var defaultLinks = [
    {source: 0, target: 1, left: false, right: true },
    {source: 1, target: 2, left: false, right: true }
    ] /// list of json; three fields


/// references

var svg = null;
var nodes = null;
var lastNodeId = null;
var links = null;

var force = null;
var drag_line = null;
var path = null;
var circle = null;

// mouse event vars   /// "mouse variables" contain nodes *ids* which are selected in corresponding events
var selected_node = null,
    selected_link = null,
    mousedown_link = null,
    mousedown_node = null,
    mouseup_node = null;

///////////// ENTRY POINT /////////////


///////////////////// INIT /////////////////////

var initGraph = function() {
    // discard all the svg tags if it exists
    if (svg !== null){
	d3.select('svg').remove();
	// initialise references:
	svg = null;
	force=null;
	path=null;
	circle=null;
	drag_line=null;
	resetMouseVars();
    }
    // set up SVG for D3
     svg = d3.select("#container") /// 'svg' refers to '<svg id="container">
	.append('svg')
	.attr('width', width)
	.attr('height', height);

// set up initial nodes and links
//  - nodes are known by 'id', not by index in array.
//  - reflexive edges are indicated on the node (as a bold black circle).
//  - links are always source < target; edge directions are set by 'left' and 'right'.

    nodes = (nodes===null) ? defaultNodes : nodes;
    lastNodeId = (lastNodeId===null) ? defaultLastNodeId : lastNodeId;
    links = (links===null) ? defaultLinks : links;

// init D3 force layout
    force = d3.layout.force()
	.nodes(nodes)
	.links(links) /// 'links' and 'nodes' should be updated when loading a new graph...
	.size([width, height])
	.linkDistance(150)
	.charge(-500)
	.on('tick', tick) /// fire 'tick()' on each 'tick' event
        .linkDistance(80)
//        .start();
    // define arrow markers for graph links /// ??
    svg.append('svg:defs').append('svg:marker')
	.attr('id', 'end-arrow')
	.attr('viewBox', '0 -5 10 10')
	.attr('refX', 6)
	.attr('markerWidth', 3)
	.attr('markerHeight', 3)
	.attr('orient', 'auto')
	.append('svg:path')
	.attr('d', 'M0,-5L10,0L0,5')
	.attr('fill', '#000');

    svg.append('svg:defs').append('svg:marker')
	.attr('id', 'start-arrow')
	.attr('viewBox', '0 -5 10 10')
	.attr('refX', 4)
	.attr('markerWidth', 3)
	.attr('markerHeight', 3)
	.attr('orient', 'auto')
	.append('svg:path')
	.attr('d', 'M10,-5L0,0L10,5')
	.attr('fill', '#000');

    // line displayed when dragging new nodes
    drag_line = svg.append('svg:path')
	.attr('class', 'link dragline hidden')
	.attr('d', 'M0,0L0,0');

// handles to link and node element groups
    path = svg.append('svg:g').selectAll('path') /// refers to all "pathes" 
    circle = svg.append('svg:g').selectAll('g'); /// refers to all "circles"

/// ':' (colon) is a name
/// e.g. 'append('svg:g')' means that it appends 'g' element which belongs to 'svg' namespace.

}    

/////////////////// END INIT (followings are subroutines)//////////////////////

function resetMouseVars() { 
  mousedown_node = null;
  mouseup_node = null;
  mousedown_link = null;
}

// update force layout (called automatically each iteration)
function tick() {
  // draw directed edges with proper padding from node centers
  path.attr('d', function(d) {
    var deltaX = d.target.x - d.source.x,
        deltaY = d.target.y - d.source.y,
        dist = Math.sqrt(deltaX * deltaX + deltaY * deltaY),
        normX = deltaX / dist,
        normY = deltaY / dist,
        sourcePadding = d.left ? 17 : 12,
        targetPadding = d.right ? 17 : 12,
        sourceX = d.source.x + (sourcePadding * normX),
        sourceY = d.source.y + (sourcePadding * normY),
        targetX = d.target.x - (targetPadding * normX),
        targetY = d.target.y - (targetPadding * normY);
    return 'M' + sourceX + ',' + sourceY + 'L' + targetX + ',' + targetY;
  });

  circle.attr('transform', function(d) {
    return 'translate(' + d.x + ',' + d.y + ')'; /// what kind of 'data' does each "circle" have?  (not yet stated.)
  });
}

// update graph (called when needed) /// called when a node|edge is clicked
function restart() {
  // path (link) group  
  path = path.data(links); /// associates 'link' data to 'path' objects
    /// 'd' points to each 'link' object

  // update existing links
    /// NOTE: the appearances of the arrows are completely managed by the css classes
  path.classed('selected', function(d) { return d === selected_link; }) /// if 'd' is the selected link, then it adds 'selected'to 'class'
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; }) /// determines the shape of an edge
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; });


  // add new links
  path.enter().append('svg:path') /// probably, 'links' is going to be updated somewhere, and then this 'enter().append()' really adds dom elements here.
    .attr('class', 'link')
    .classed('selected', function(d) { return d === selected_link; }) /// 'd' here refers to one json of such form as {source: foo, target: bar, left/right: bool}
    .style('marker-start', function(d) { return d.left ? 'url(#start-arrow)' : ''; })
    .style('marker-end', function(d) { return d.right ? 'url(#end-arrow)' : ''; })
    .on('mousedown', function(d) {
      if(d3.event.ctrlKey) return; /// this does not work since it is disabled (commented out the event handling part)  That is, the following codes are always executed.

      // select link
      mousedown_link = d;  /// 'mousedon_link' points to the link 'd'.
      if(mousedown_link === selected_link) selected_link = null; /// if the link is clicked twice, this removes the sellection
      else selected_link = mousedown_link;
      selected_node = null; /// refreshes previous node sellection 
      restart(); /// this means whenever click an edge, 'app.js' updates the whole graph (almost redraws the entire graphs...) 
    });

  // remove old links
  path.exit().remove();

/// node part ///
    
  // circle (node) group
  // NB: the function arg is crucial here! nodes are known by id, not by index!
  circle = circle.data(nodes, function(d) { return d.id; });

  // update existing nodes (reflexive & selected visual states)
  circle.selectAll('circle')
    .style('fill', function(d) { return (d === selected_node) ? d3.rgb(colors(d.id)).brighter().toString() : colors(d.id); })
    .classed('reflexive', function(d) { return d.reflexive; })
//    .append('svg:text').text(function(d) {return d.desc;});
    
    // update texts
   circle.selectAll('.desc').text(function(d){return d.desc;});



  // add new nodes
  var g = circle.enter().append('svg:g');
  // var Aradius = 18;
  // var Bradius = 9;
  g.append('svg:circle')
    .attr('class', function(d) { return d.type+"Node" })  // set 'class' according to the type of the node
    .attr('r', function(d) { return (d.type === "A") ? constants.radius.A : constants.radius.B; }) // A-nodes are larger than B-nodes
    .style('fill', function(d) { return (d === selected_node) ? d3.rgb(colors(d.id)).brighter().toString() : colors(d.id); })
    .style('stroke', function(d) { return d3.rgb(colors(d.id)).darker().toString(); })
    .classed('reflexive', function(d) { return d.reflexive; })
    .on('mouseover', function(d) {
      if(!mousedown_node || d === mousedown_node) return; /// if some other node is already clicked... (this happens when one tries to draw an arrow)
      // enlarge target node
      d3.select(this).attr('transform', 'scale(1.5)');
    })
    .on('mouseout', function(d) {
      if(!mousedown_node || d === mousedown_node) return;
      // unenlarge target node
      d3.select(this).attr('transform', '');
    })
    .on('mousedown', function(d) {
      if(d3.event.ctrlKey) return; ///If ctrl is pressed, then it goes to "drag mode"

      // select node
      mousedown_node = d;
      if(mousedown_node === selected_node) selected_node = null;
      else selected_node = mousedown_node;
      selected_link = null;

	// show/hide balloon
	
	// closure
	// signature: toggleBalloon(this,content);
	// defined in 'balloon.editor.js
	
	// toggleBalloon(this,(function setContent () {
	//     var content = '<input type="text" value="'+d.desc+'"/>';
	//     var hey = "'hey'"
	//     content += '<input type="button" onclick="prompt('+hey+')" value="Enter">';
	//     return content;
	// })());

	toggleBalloon_d(this,(function setContent () {
	    var content = '<input type="text" id="'+d.id+'" value="'+d.desc+'"/>';
//	    var hey = "'hey'"
//	    content += '<input type="button" onclick="prompt('+hey+')" value="Enter">';
//	    content += '<input type="button" onclick="setdata('+d+') value="Enter">';
	    return content;
	})(),d);
	// function setdata(dataobj){
	//     dataobj.desc = 
	
      // reposition drag line
      drag_line
        .style('marker-end', 'url(#end-arrow)')
        .classed('hidden', false)
        .attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + mousedown_node.x + ',' + mousedown_node.y);

      restart();
    })
    .on('mouseup', function(d) {
      if(!mousedown_node) return;

      // needed by FF
      drag_line
        .classed('hidden', true)
        .style('marker-end', '');

      // check for drag-to-self
      mouseup_node = d;
      if(mouseup_node === mousedown_node) { resetMouseVars(); return; }

      // unenlarge target node
      d3.select(this).attr('transform', '');

      // add link to graph (update if exists)
      // NB: links are strictly source < target; arrows separately specified by booleans
      var source, target, direction;
      if(mousedown_node.id < mouseup_node.id) {
        source = mousedown_node;
        target = mouseup_node;
        direction = 'right';
      } else {
        source = mouseup_node;
        target = mousedown_node;
        direction = 'left';
      }

      var link;
      link = links.filter(function(l) {
        return (l.source === source && l.target === target);
      })[0];

      if(link) {
        link[direction] = true;
      } else {
        link = {source: source, target: target, left: false, right: false};
        link[direction] = true;
        links.push(link);
      }

      // select new link
      selected_link = link;
      selected_node = null;
      restart();
    });
    

  // show node 'desc'
  
  var text_attr = g.append('svg:text')
      .attr('x', function(d) { return (d.type === "A") ? constants.radius.A+4 : constants.radius.B+4})
      .attr('y', 4)
      .attr('class', 'desc');
    
    text_attr.text(function(d) { //return (d.desc === undefined) ? "" : d.desc; });
	  if(d.desc!==undefined){
	      return d.desc
	  }else if(d.type==="A"){
	      d.desc = prompt("Enter the short description for this node","Node ID:"+d.id);
	       return d.desc;
	      // globals.newID = d.id;
	      // $( "#dialog-form" ).dialog( "open" );
	      //function() {$( "#dialog-form" ).dialog( "open" ) }//"ok" // function to be implemented
	  }
	  return "" // if the node is B-node, append no texts
	  });
//    g.append('image').attr('xlink:href',"wilylogo.jpg").attr('height','50px').attr('width','50px').attr('x',-10).attr('y',-10);
    // g.append('foreignObject')
    // 	.attr('width', 100).attr('height',100)
    // 	.append('xhtml:body')
    // 	.style("font", "14px 'Helvetica Neue'")
    // 	.html("<input type='text'>He!</>")
    // ;

  // remove old nodes
  circle.exit().remove();

  // set the graph in motion
  force.start();
}

function CallPrompt(){

  // This requires short desription for newly created node.
  // thanks to jQuery UI, this looks fancy enough
}

function mousedown() {
  // prevent I-bar on drag
  d3.event.preventDefault();
  
  // because :active only works in WebKit?
  svg.classed('active', true);
    //prompt("hey");
  if(d3.event.ctrlKey || mousedown_node || mousedown_link) return;

  // insert new node at point

  // determine the type of new node
  var nodeType = (document.getElementById('ANode').checked) ? "A" : "B";
  var point = d3.mouse(this);
  var node = {id: ++lastNodeId, reflexive: false, type: nodeType}; //danger!  lastNodeID should be preserved as an html tag.
  node.x = point[0];
  node.y = point[1];
  nodes.push(node); /// this appends a datum to 'node' array

  restart();
}

function mousemove() { /// handles arrow drawing
  if(!mousedown_node) return;

  // update drag line
  drag_line.attr('d', 'M' + mousedown_node.x + ',' + mousedown_node.y + 'L' + d3.mouse(this)[0] + ',' + d3.mouse(this)[1]);

  restart();
}

function mouseup() { /// handles arrow drawing, link making
  if(mousedown_node) {
    // hide drag line
    drag_line
      .classed('hidden', true)
      .style('marker-end', '');
  }

  // because :active only works in WebKit?
  svg.classed('active', false);

  // clear mouse event vars
  resetMouseVars();
}

function spliceLinksForNode(node) {
  var toSplice = links.filter(function(l) {
    return (l.source === node || l.target === node);
  });
  toSplice.map(function(l) {
    links.splice(links.indexOf(l), 1);
  });
}

// only respond once per keydown
var lastKeyDown = -1;

function keydown() {
    /// prevents the usual behaviour of browsers regarding keydown event
    /// ... meaning that you cannot interact with the browser using keyboard!
    
    //  d3.event.preventDefault(); for the time being, I have disabled this functionallity.

  if(lastKeyDown !== -1) return;
  lastKeyDown = d3.event.keyCode;

  // ctrl
  if(d3.event.keyCode === 17) {
    circle.call(force.drag);
    svg.classed('ctrl', true);
  }

  if(!selected_node && !selected_link) return;

    /// behaves according to the key press given
    
  // switch(d3.event.keyCode) {
  //   case 8: // backspace
  //   case 46: // delete
  //     if(selected_node) {
  //       nodes.splice(nodes.indexOf(selected_node), 1);
  //       spliceLinksForNode(selected_node);
  //     } else if(selected_link) {
  //       links.splice(links.indexOf(selected_link), 1);
  //     }
  //     selected_link = null;
  //     selected_node = null;
  //     restart();
  //     break;
  //   case 66: // B
  //     if(selected_link) {
  //       // set link direction to both left and right
  //       selected_link.left = true;
  //       selected_link.right = true;
  //     }
  //     restart();
  //     break;
  //   case 76: // L
  //     if(selected_link) {
  //       // set link direction to left only
  //       selected_link.left = true;
  //       selected_link.right = false;
  //     }
  //     restart();
  //     break;
  //   case 82: // R
  //     if(selected_node) {
  //       // toggle node reflexivity
  //       selected_node.reflexive = !selected_node.reflexive;
  //     } else if(selected_link) {
  //       // set link direction to right only
  //       selected_link.left = false;
  //       selected_link.right = true;
  //     }
  //     restart();
  //     break;
  // }
}

function keyup() {
  lastKeyDown = -1;

  // ctrl
  if(d3.event.keyCode === 17) {
    circle
      .on('mousedown.drag', null)
      .on('touchstart.drag', null);
    svg.classed('ctrl', false);
  }
}


initGraph();

var startGraph = function(){
// app starts here
    svg.on('mousedown', mousedown)
	.on('mousemove', mousemove)
	.on('mouseup', mouseup);
    d3.select(window)
	.on('keydown', keydown)
	.on('keyup', keyup);
    restart();
    // render math for the first time
    //    d3.selectAll('circle').each(function(){renderMathIn(this);});
}

startGraph();

//d3.selectAll('circle').each(function(){renderMathIn(this);});
