// loads graph
// currently, as the specification of 'saveGraph', this replace entire curret 'svg' tag in the DOM with the new graph
// this funciton is called when the 'Load graph' button is pressed.
function loadGraph(event){
    console.log("start loading svg file...");
    var files = event.target.files;
    var reader = new FileReader();

    reader.onload = function(evt) {
	console.log("reader is on load...");
	var result = evt.target.result;
	
	$("svg").remove(); // erase all the svg objects
	$("#container").append(result); // append the newly loaded svg tag

	console.log("load successful");
    }
    reader.readAsText(files[0]); // start reading files, invoking 'onload' event
}
