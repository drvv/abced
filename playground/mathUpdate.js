var svgNameSpace = "http://www.w3.org/2000/svg";

var param = {
    baseline: 1.8, //ex
    spacing: 1.8, // should be measured in 'em'...
    offset: 10+4
}

function updateMath(svgCirc) {
    // create/init "buffer"
    if (d3.select(svgCirc).datum().type === "B"){
	return
    }
    var txt = d3.select(svgCirc).datum().desc;	// original param. is 'txt'
    
    var d3_buffer = d3.select('#buffer');
    if (d3_buffer.empty()){
	d3_buffer = d3.select('body').append('div').attr('id',"buffer");
	d3_buffer.append('div').attr('id',"mathjaxOutput")
	    .classed('buffer',true);
	d3_buffer.append('svg:svg').attr('id',"svgOutput")
	    .classed('buffer',true)
	    .attr('width',"200px").attr('height',"100px");
	d3_buffer.append('p').attr('id',"ruler")
	    .classed('buffer',true)
	    .style('height',"1ex").text("x");
    }
    // dot row input into '#mathjaxOutput' and get ref to the *tag*
    var $output = $( '#mathjaxOutput' ).html(txt);
//    $output.css("display","none");
//    $buffer.css("display","none");
    //$output.html(txt);

    // init mathjax outputs in svg (final target)
    $(svgCirc).siblings('.mathjaxSvg').remove();

    // typeset math (the result shown in '#mathjaxOutput') 
    MathJax.Hub.Queue(["Typeset", MathJax.Hub, "mathjaxOutput"]);

    // modifies tags AFTER typesetting
    // the outcome is stored in '#svgOutput', instead of '#mathjaxOutput'
    MathJax.Hub.Queue(
	function() {
	    // get tag "chunks" in array
	    var parsed = $.parseHTML($output.html()); 
	    // remove tags except for 'span' and '#text'
	    var filtered = $.grep(parsed, function(elm,id){
		return $(elm).attr('class') === "MathJax_SVG"||elm.nodeName === "#text";
	    });

	    // init '#svgOutput'
	    var $svgOutput = $( '#svgOutput' ).html('');

	    // remove existing mathjax output in svg
	    // $('.mathjaxSvg').remove();
	    // $('.nonMath').remove();

	    // format tags
	    $.each(filtered, function(i, elm){
		if (elm.nodeName === "SPAN"){
		    var svg = $(elm).children().attr('class',"mathjaxSvg")
		    
		    console.log(svg.width()); // show size in 'ex' 
		    
		    var widthpx = ex2px(svg.width(),'#ruler'); // convert
		    var heightpx = ex2px(svg.height(),'#ruler');
		    
		    svg.attr('width', widthpx); // set length as css style
		    svg.attr('height', heightpx);
		    
		    // align according to the baseline
		    svg.attr('y',ex2px(-param.baseline,'#ruler'));
		    
		    elm = filtered[i] = svg[0]; // extract 'svg'
		    // this 'elm' is jQuery object
		    
		}else{ // should be '#text'
		    // elm = filtered[i] = $("<text>"+elm.nodeValue+"</text>");
		    var bareText = elm.nodeValue;
		    var txtNode = document.createTextNode(bareText);
		    var svgtxt = document.createElementNS(svgNameSpace,'text');

		    svgtxt.setAttributeNS(null,'class',"mathjaxSvg"); // discriminate between original '<text>'
		    svgtxt.appendChild(txtNode); // add text node (i.e. set text content)
		    
		    elm = filtered[i] = svgtxt;
		    // this 'elm' is 'node' object
		}
		
		//$( '#svgOutput' ).append(elm); // create the actual node to measure its width
		// alternative inprementation without jQuery
		document.getElementById("svgOutput").appendChild(elm);

		var type = elm.nodeName;
		console.log(type+" width(): "+$(elm).width());
		console.log(type+" attr('width'): "+$(elm).attr('width'));

		// get 'x' coordinates
		elm = $(elm);	// maps into jQuery object (to use '.width()' )

		// if 'elm' is a text node, then it has nonzero '.width()'
		// should be set to attr('width') for uniformation
		
		if (elm.width()!==0){
		    elm.attr('width',elm.width());
		}
		
		elm.attr("x", (function x(n) {
		    console.log("width: "+$(filtered[n]).attr('width'));
		    // console.log("n:"+n);
		    // console.log("i:"+i); // interesting difference
		    if(n===0) { return param.offset; }
		    return x(n-1) + parseInt($(filtered[n-1]).attr('width')) + param.spacing;
		})(i))
	    });
	    // make #svgOutput invisible
	    //$buffer.css("display","none");
	    SVGappendTo(svgCirc);
	});
    // draw within 'svg'
    // MathJax.Hub.Queue(
    // 	function(){
    // 	    $( '#canvas' ).html('');
    // 	    $( '#svgOutput' ).children().appendTo( '#canvas' );
    // 	});
    // MathJax.Hub.Queue(SVGappendTo(svgCirc)); executed before typestting
}

// entry function
function renderMathIn(svgCirc) { // parameter should be '<circle>' element
    //    MathJax.Hub.Queue(updateMath(svgCirc.d().desc));
    MathJax.Hub.Queue(updateMath(svgCirc));
    //MathJax.Hub.Queue(SVGappendTo(svgCirc)); executed before rendering!
    // function(svgelm) {...} does not work!
}

function SVGappendTo(svgCirc) { 
    var parent = svgCirc.parentElement; // returns 'g' object enclosing 'circle'
    //    d3.select(parent).append('svg:g').append('#svgOutput');
    var svgOutput = document.getElementById("svgOutput")
    // get alist of elements
    var outputs = document.getElementById("svgOutput").children;

    var originalLength = outputs.length;
    
    // for(var i=0; i<originalLength; i++)	{
    // 	console.log(i+"th child:");
    // 	console.log(outputs[i]);
    // 	parent.appendChild(outputs[i]); // this deletes ith element when called
    // }

    // set text invisible
    // for (var i=0; i<parent.children.length; i++){
    // 	if (parent.children[i].className === "desc"){
    // 	    parent.children[i].style.display = "none";
    // 	}
    // }
    
    // hide 'desc' for a node
    d3.select(parent).selectAll('.desc').style('display','none');
    
    // init
    var count = 0;
    while(outputs.length>0){
	console.log(count+++"th element");
//	$(outputs[0]).attr('class','mathjax_made');
	parent.appendChild(outputs[0]);
    }
}

function ex2px (ex, refObj){
    var exInpx = $(refObj).height(); // get 1ex in px
    return ex*exInpx;
}
