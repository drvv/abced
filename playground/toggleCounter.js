var gen = function(){
    var shown=true; // gives 'false' fot the first call
    return function(){
	shown = !shown
	return shown; // gives 'false' for the first call
    }
	   
}

//var shown = gen();

var tipContents = "<span>toggle!</span>";

// function toggle(obj) {
  
//     if (obj.shown===undefined){
// 	obj.shown = gen().bind(obj); // this does not bind function after all!!!
// 	}
//     var tf = obj.shown();
//     var idd = obj.id; 
//     if (!tf){
// 	$(idd).showBalloon({contents: tipContents});
//     }else{
// 	$(idd).hideBalloon();
//     }
// }

// function toggle_obsolete(){
//     if (typeof shown==="undefined"){
// 	shown = gen(); // share counters
// }
//     var tf = shown();
//     var obj = $(this);
//     if(!tf){
// 	obj.showBalloon({contents: tipContents});
// 	}
//     else{
// 	obj.hideBalloon()
// 	}

// }

// function toggle_id(obj){
//     var sel = $(obj)
    
//     if (typeof shown === "undefined"){
// 	shown = gen();
//     }
	
//     var tf = shown();
    
//     if(!tf){
// 	sel.showBalloon({contents: tipContents});
//     }else{
// 	sel.hideBalloon();
//     }
// }
function toggle(object){
    var obj = object // DOM object
    var jqobj = $(obj); // jQuery object

    if (typeof object.shown === "undefined"){
	object.shown = gen().bind(); // register function to the element
	}
  //    var tf = object.shown();
    
    if(!object.shown()){
	jqobj.showBalloon({contents: tipContents});
    }else{
	jqobj.hideBalloon();
    }
}
