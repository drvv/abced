// saves graph
// currently, this saves entire 'svg' tag
function saveGraph(){
    $("svg").attr({ version: '1.1' , xmlns:"http://www.w3.org/2000/svg"}); // without this line, the browser only show the xml data in a newly created window
    
    var svgContent = $("#container").html();

    var b64Data = Base64.encode(svgContent);

    // add the graph as an image -- it does NOT REALLY work
    //$("body").append($("<img src='data:image/svg+xml;base64,\n"+b64Data+"' alt='file.svg'/>"));
    
    // ... instead, this creates a link to .svg file
    $("#downloadLinkContainer").append($("<a href-lang='image/svg+xml' href='data:image/svg+xml;base64,\n"+b64Data+"' title='file.svg'>Download</a>"))
    
    var uri = "data:image/svg+xml;base64,\n"+b64Data ;
    
    window.open(uri); // PROMBLEM: the browser might not interprete UTF-8 characters in svg. (this is the case for mine.)
}
