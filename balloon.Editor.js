// var counter_gen = (function(){  
//     var shown = true;
//     return function(){
// 	shown = !shown;
// 	return shown;
// 	}
//     })();

var counter_gen = function(){
    var shown = true;
    return function(){
	shown = !shown;
	return shown;
	}
    };

var counter_gen_d = function(d){
    var dref = d; // for data reference
    var shown = true;
    return {
	showFlag: function(){
	    shown = !shown;
	    return shown;
	},
	showDref: function(){
	    return dref;
	}
    }
}
	

var htmlContent = "<span>hello!</span>";

function toggleBalloon(object, html){
    var obj = object;		// svg object
    var jqobj = $(obj);
    
    if (typeof obj.toggle === "undefined"){
//	obj.toggle = counter_gen().bind();
    }

    var tf = obj.toggle();

    // creates html content
    // var cont = "";
    // cont += '<input type="text">'+obj.text

    if (!tf){
	jqobj.showBalloon({contents: html});
    }else{
	jqobj.hideBalloon();
    }
}

function toggleBalloon_d(object, html,d){
    var obj = object;		// svg object ref.
    var jqobj = $(obj);
    var counterObj = counter_gen_d(d);
    if (typeof obj.toggle === "undefined"){
//	obj.toggle = counter_gen_d(d).showFlag.bind();
	obj.toggle = counterObj.showFlag.bind();
	obj.d = counterObj.showDref.bind();
    }

    var tf = obj.toggle();
//    obj.d.desc = "cool!";
//    console.log(obj.d().desc);
    //    obj.d().desc = "cool!";
//    d3.select(
    if (!tf){
	jqobj.showBalloon({contents: html, showDuration:100, showAnimation: function(d) { this.fadeIn(d);}});
    }else{
	var inputText = document.getElementById(obj.d().id).value;
	obj.d().desc = inputText; // replace 'desc' data  which is associated with the svg object, with the value of the text input
	// the actual update is going to be fired in 'app.js'
	
	// call MathJax and display
	renderMathIn(obj);
	
	jqobj.hideBalloon();
    }
}
