///
/// special thanks to 
/// http://thiscouldbebetter.wordpress.com/2012/12/18/loading-editing-and-saving-a-text-file-in-html5-using-javascrip/
///

function saveGraphAsText(name,data) {
    /// save file as a text file
    
    var fileNameToSaveAs = name;
    var textData = JSON.stringify(data);
    var blobData = new Blob([textData], {type: 'text/javascript'})

    /// creates "virtual" link element
    var downloadLink = document.createElement("a");
    downloadLink.download = fileNameToSaveAs;
    downloadLink.innerHTML = "Download File";

    if (window.webkitURL != null)
    {
	// Chrome allows the link to be clicked
	// without actually adding it to the DOM.
	downloadLink.href = window.webkitURL.createObjectURL(blobData);
    }
    else
    {
	// Firefox requires the link to be added to the DOM
	// before it can be clicked.
	downloadLink.href = window.URL.createObjectURL(textFileAsBlob);
	downloadLink.onclick = destroyClickedElement;
	downloadLink.style.display = "none";
	document.body.appendChild(downloadLink);
    }

    downloadLink.click();
}

/// for Firefox
function destroyClickedElement(event)
{
	document.body.removeChild(event.target);
}

function loadFileAsText(fileToLoad)
{
    //var fileToLoad = document.getElementById("fileToLoad").files[0];
    
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent) 
    {
	var textFromFileLoaded = fileLoadedEvent.target.result;
	//document.getElementById("inputTextToSave").value = textFromFileLoaded;
	var graphObj = JSON.parse(textFromFileLoaded);
	// update graph data
	nodes = graphObj.nodes;
	lastNodeId = graphObj.lastNodeId;
	links = graphObj.links;
	initGraph();	
    };
    fileReader.readAsText(fileToLoad, "UTF-8");
}
