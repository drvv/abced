function parseLinks(links){
    // init new array
    var parsedLinks = new Array();
    
    for (var i=0; i<links.length;i++)
    {
	// init vars
	var sindex=0;
	var tindex=0;
	var left=false;
	var right=false;
	
	// init ref var
	var lnki = links[i];

	// register data
	sindex=lnki.source.index;
	tindex=lnki.target.index;
	left=lnki.left;
	right=lnki.right;

	// create obj in the array
	parsedLinks[i] = {"source": sindex, "target": tindex, "left": left, "right": right}
    }
    return parsedLinks;
}
